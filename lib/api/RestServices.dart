import 'package:flutter_alor/models/ChangePasswordModel.dart';
import 'package:flutter_alor/models/DummyModel.dart';
import 'package:flutter_alor/models/KomplainModel.dart';
import 'package:flutter_alor/models/LoginModel.dart';
import 'package:flutter_alor/models/PegawaiModel.dart';
import 'package:flutter_alor/models/RiwayatPegawaiModel.dart';
import 'package:flutter_alor/models/UserModel.dart';
import 'package:http/http.dart' as http;

class RestServices {
  static var client = http.Client();
  static var baseUrl = "http://128.199.185.131/simpeg/api/";
  static var storageUrl = "http://128.199.185.131/simpeg/storage/";

  static Future<LoginModel> postLogin(String nip, String password) async {
    var response = await client.post(
      Uri.parse(baseUrl + 'login?nip=$nip&password=$password'),
    );
    if (response.statusCode == 200) {
      var str = response.body;
      print('loginModel : ${str.length}');
      return loginModelFromJson(str);
    } else {
      print('loginModel : $response');
      return null;
    }
  }

  static Future<ChangePasswordModel> postChangePassword(
      String nip, String password) async {
    var response = await client.post(
      Uri.parse(baseUrl + 'save_password?nip=$nip&password=$password'),
    );
    if (response.statusCode == 200) {
      var str = response.body;
      print('ChangePasswordModel : ${str.length}');
      return changePasswordModelFromJson(str);
    } else {
      print('ChangePasswordModel : $response');
      return null;
    }
  }

  static Future<KomplainModel> postKomplain(
      String nip, String judul, String isi, String attachment) async {
    var request = http.MultipartRequest(
      'POST',
      Uri.parse(baseUrl + "send_message"),
    );

    request.fields["nip"] = nip;
    request.fields["judul"] = judul;
    request.fields["isi"] = isi;

    //multipart file
    var file = await http.MultipartFile.fromPath("attachment", attachment);
    request.files.add(file);

    //send request
    var response = await request.send();
    if (response.statusCode == 200) {
      var responseData = await response.stream.toBytes();
      var responseString = String.fromCharCodes(responseData);
      print(responseString);
      return komplainModelFromJson(responseString);
    } else {
      return null;
    }
  }

  //get response

  static Future<UserModel> fetchUser(String nip) async {
    var response = await client.get(
      Uri.parse(baseUrl + 'user/get/$nip'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('UserModel : ${str.length}');
      return userModelFromJson(str);
    } else {
      print('UserModel : $response');
      return null;
    }
  }

  static Future<PegawaiModel> fetchPegawai(String nip) async {
    var response = await client.get(
      Uri.parse(baseUrl + 'pegawai/$nip'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('PegawaiModel : ${str.length}');
      return pegawaiModelFromJson(str);
    } else {
      print('PegawaiModel : $response');
      return null;
    }
  }

  static Future<RiwayatPegawaiModel> fetchRiwayatPegawai(String nip) async {
    var response = await client.get(
      Uri.parse(baseUrl + 'riwayat/$nip'),
    );

    if (response.statusCode == 200) {
      var str = response.body;
      print('RiwayatPegawaiModel : ${str.length}');
      return riwayatPegawaiModelFromJson(str);
    } else {
      print('RiwayatPegawaiModel : $response');
      return null;
    }
  }

  // static Future<LoginModel> postLogin(String username, String password) async {
  //   var request = http.MultipartRequest(
  //     'POST',
  //     Uri.parse(baseUrl + "login"),
  //   );

  //   request.fields["username"] = username;
  //   request.fields["password"] = password;

  //   var response = await request.send();
  //   var responseData = await response.stream.toBytes();
  //   var responseString = String.fromCharCodes(responseData);
  //   print('register = $responseString');
  //   if (response.statusCode == 200) {
  //     var result = loginModelFromJson(responseString);
  //     return result;
  //   } else {
  //     var result = loginModelFromJson(responseString);
  //     print('false postlogin');
  //     return result;
  //   }
  // }
}
