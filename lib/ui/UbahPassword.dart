import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/AuthController.dart';
import 'package:flutter_alor/controllers/PasswordController.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';

class UbahPassword extends StatelessWidget {
  const UbahPassword({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final TextEditingController passwordLama = TextEditingController();
    final TextEditingController passwordBaru = TextEditingController();

    final passwordController = Get.put(PasswordController());
    final authController = Get.put(AuthController());
    return Scaffold(
      appBar: AppBar(
        title: Text('Formulir Ubah Password'),
      ),
      body: Obx(
        () => Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Column(
            children: [
              TextField(
                obscureText: true,
                onChanged: (value) {
                  passwordController.passwordLama.value = value;
                },
                controller: passwordLama,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color: Colors.grey),
                  suffixIcon: Icon(
                    LineIcons.edit,
                  ),
                  labelText: 'Password Baru',
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue)),
                ),
              ),
              TextField(
                obscureText: true,
                onChanged: (value) {
                  passwordController.passwordBaru.value = value;
                },
                controller: passwordBaru,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color: Colors.grey),
                  suffixIcon: Icon(
                    LineIcons.edit,
                  ),
                  labelText: 'Konfirmasi Password Baru',
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue)),
                ),
              ),
              SizedBox(height: 10),
              passwordController.passwordLama.value.isNotEmpty &&
                      passwordController.passwordBaru.value.isNotEmpty
                  ? Text(
                      passwordController.passwordLama.value ==
                              passwordController.passwordBaru.value
                          ? 'password sama'
                          : 'password tidak sama',
                      style: TextStyle(
                          color: passwordController.passwordLama.value ==
                                  passwordController.passwordBaru.value
                              ? Colors.green
                              : Colors.red),
                    )
                  : SizedBox.shrink(),
              SizedBox(height: 10),
              MaterialButton(
                shape: RoundedRectangleBorder(
                  borderRadius: new BorderRadius.circular(5),
                ),
                // color: Color(0xFF2D665F),
                color: Colors.blue,
                minWidth: MediaQuery.of(context).size.width,
                height: 50.0,
                child: Text(
                  'Ganti Password',
                  style: TextStyle(color: Colors.white, fontSize: 16),
                ),
                onPressed: () async {
                  // print(
                  //     '${usernameController.text}, ${passwordController.text}');
                  if (passwordLama.text == passwordBaru.text) {
                    passwordController.changePassword(
                        authController.userModel.value.nip, passwordBaru.text);
                    
                  } else {
                    Get.snackbar('not match', 'password tidak sama',
                        snackPosition: SnackPosition.BOTTOM,
                        backgroundColor: Colors.red[900],
                        colorText: Colors.white);
                  }

                  //Send to API
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
}
