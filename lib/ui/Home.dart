import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/AuthController.dart';
import 'package:flutter_alor/controllers/HomeController.dart';
import 'package:flutter_alor/controllers/KomplainController.dart';
import 'package:flutter_alor/ui/Dashboard/Dashboard.dart';
import 'package:flutter_alor/ui/Komplain/Komplain.dart';
import 'package:flutter_alor/ui/Settings/Settings.dart';
import 'package:get/get.dart';

class Home extends StatelessWidget {
  const Home({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final homeController = Get.put(HomeController());
    final authController = Get.put(AuthController());
    final komplainController = Get.put(KomplainController());

    return Obx(
      () => Scaffold(
        appBar: AppBar(
          title: Text(homeController.currentIndex.value == 0
              ? 'Dashboard'
              : homeController.currentIndex.value == 1
                  ? 'Komplain'
                  : 'Settings'),
        ),
        body: IndexedStack(
          index: homeController.currentIndex.value,
          children: [
            Dashboard(),
            Komplain(),
            Settings(),
          ],
        ),
        bottomNavigationBar: BottomNavigationBar(
          currentIndex: homeController.currentIndex.value,
          backgroundColor: Colors.blue,
          selectedIconTheme: IconThemeData(color: Colors.white),
          selectedItemColor: Colors.white,
          selectedLabelStyle: TextStyle(
            fontSize: 12,
            height: 2.0,
          ),
          unselectedLabelStyle: TextStyle(
            fontSize: 12,
            height: 2.0,
          ),
          unselectedItemColor: Colors.black54,
          showSelectedLabels: true,
          showUnselectedLabels: true,
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(top: 3.0),
                child: Icon(Icons.badge),
              ),
              label: "Dashboard",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(top: 3.0),
                child: Icon(Icons.chat_rounded),
              ),
              label: "Komplain",
            ),
            BottomNavigationBarItem(
              icon: Padding(
                padding: const EdgeInsets.only(top: 3.0),
                child: Icon(CupertinoIcons.gear_alt_fill),
              ),
              label: "Settings",
            ),
          ],
          onTap: (index) {
            homeController.currentIndex.value = index;
          },
        ),
        floatingActionButton: homeController.currentIndex.value == 1
            ? FloatingActionButton.extended(
                onPressed: () {
                  komplainController.addKomplain(
                      authController.userModel.value.nip,
                      komplainController.judulFile.value,
                      komplainController.isiFile.value,
                      komplainController.imageFile.value);
                },
                label: Row(
                  children: [
                    Icon(Icons.add_comment),
                    SizedBox(width: 10),
                    Text('Kirim Komplain')
                  ],
                ),
              )
            : SizedBox.shrink(),
      ),
    );
  }
}
