import 'dart:io';

import 'package:dotted_border/dotted_border.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/KomplainController.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:line_icons/line_icons.dart';

class Komplain extends StatelessWidget {
  const Komplain({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final komplainController = Get.put(KomplainController());

    final TextEditingController judul = TextEditingController();
    final TextEditingController isi = TextEditingController();

    return Obx(
      () => SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10),
          child: Column(
            children: [
              TextField(
                onChanged: (value) {
                  // passwordController.passwordLama.value = value;
                  komplainController.judulFile.value = value;
                },
                controller: judul,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color: Colors.grey),
                  suffixIcon: Icon(
                    LineIcons.italic,
                  ),
                  labelText: 'Judul',
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue)),
                ),
              ),
              TextField(
                onChanged: (value) {
                  // passwordController.passwordLama.value = value;
                  komplainController.isiFile.value = value;
                },
                controller: isi,
                decoration: InputDecoration(
                  labelStyle: TextStyle(color: Colors.grey),
                  suffixIcon: Icon(
                    LineIcons.at,
                  ),
                  labelText: 'Isi',
                  enabledBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.grey)),
                  focusedBorder: UnderlineInputBorder(
                      borderSide: BorderSide(color: Colors.blue)),
                ),
              ),
              SizedBox(height: 10),
              showImagePicker(context, komplainController),
            ],
          ),
        ),
      ),
    );
  }

  showImagePicker(BuildContext context, KomplainController komplainController) {
    return DottedBorder(
      dashPattern: [10, 10, 10, 10],
      color: Colors.grey,
      strokeWidth: 2,
      radius: Radius.circular(100),
      borderType: BorderType.Rect,
      child: SizedBox(
        width: MediaQuery.of(context).size.width,
        height: 200,
        child: komplainController.imageFile.value.isEmpty
            ? InkWell(
                onTap: () {
                  komplainController.getLocalImage(ImageSource.gallery);
                },
                child: Icon(
                  Icons.image,
                  color: Colors.blue,
                  size: 100,
                ),
              )
            : Stack(
                alignment: AlignmentDirectional.bottomCenter,
                children: [
                  SizedBox(
                      width: MediaQuery.of(context).size.width,
                      height: 200,
                      child: Image.file(
                        File(komplainController.imageFile.value),
                        fit: BoxFit.fitHeight,
                      )),
                  MaterialButton(
                    color: Colors.black54,
                    onPressed: () {
                      komplainController.getLocalImage(ImageSource.gallery);
                    },
                    child: Row(
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        Text(
                          'Ganti Gambar ',
                          style: TextStyle(
                            color: Colors.white,
                          ),
                        ),
                        Icon(
                          Icons.folder,
                          color: Colors.white,
                        )
                      ],
                    ),
                  ),
                ],
              ),
      ),
    );
  }
}
