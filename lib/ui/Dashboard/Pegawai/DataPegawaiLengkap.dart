import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:get/get.dart';

import 'InformasiPegawai.dart';
import 'InformasiPersonal.dart';

class DataPegawaiLengkap extends StatelessWidget {
  const DataPegawaiLengkap({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pegawaiController = Get.put(PegawaiController());
    return Scaffold(
      appBar: AppBar(
        title: Text('IDENTITAS PEGAWAI'),
      ),
      body: Column(
        children: [
          InkWell(
            onTap: () {
              Get.to(() => InformasiPersonal());
            },
            child: Card(
              child: ListTile(
                title: Text('Informasi Personal'),
                subtitle: Text('Informasi Personal Pegawai'),
                trailing: Icon(Icons.chevron_right),
              ),
            ),
          ),
          InkWell(
            onTap: () => Get.to(() => InformasiPegawai()),
            child: Card(
              child: ListTile(
                title: Text('Informasi Pegawai'),
                subtitle: Text('Data Lengkap Pegawai'),
                trailing: Icon(Icons.chevron_right),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
