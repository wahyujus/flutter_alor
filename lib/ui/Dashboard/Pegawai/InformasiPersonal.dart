import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:get/get.dart';

class InformasiPersonal extends StatelessWidget {
  const InformasiPersonal({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pegawaiController = Get.put(PegawaiController());
    return Scaffold(
      appBar: AppBar(
        title: Text('Informasi Personal'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              child: ListTile(
                title: Text('NIP Lama'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.nipLama ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('NIP Baru'),
                subtitle: Text(pegawaiController.pegawaiModel.value.nip ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Nama Lengkap'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.namaLengkap ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Tempat Lahir'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.tempatLahir ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Tanggal Lahir'),
                subtitle: Text(pegawaiController.full
                    .format(pegawaiController.pegawaiModel.value.tanggalLahir)
                    .toString()),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Jenis Kelamin'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.jenisKelamin ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Agama'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.agama ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Status Pernikahan'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.statusNikah ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Suku Bangsa'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.sukuBangsa ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Gol. Darah'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.golonganDarah ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Alamat'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.alamat ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Provinsi'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.provinsi ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Kota/Kabupaten'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.kabupaten ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Kecamatan'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.kecamatan ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Kelurahan/Desa'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.desa ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('RT/RW'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.alamatRt ?? '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Kode Pos'),
                subtitle:
                    Text(pegawaiController.pegawaiModel.value.kodepos ?? '-'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
