import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:get/get.dart';

class InformasiPegawai extends StatelessWidget {
  const InformasiPegawai({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pegawaiController = Get.put(PegawaiController());
    return Scaffold(
      appBar: AppBar(
        title: Text('Informasi Pegawai'),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: [
            Card(
              child: ListTile(
                title: Text('Satuan Kerja'),
                subtitle: Text(pegawaiController
                        .pegawaiModel.value.dataLengkap.satuanKerja ??
                    '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Satuan Kerja Anak'),
                subtitle: Text(pegawaiController
                        .pegawaiModel.value.dataLengkap.satuanKerjaAnak ??
                    '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Status Pegawai'),
                subtitle: Text(pegawaiController
                        .pegawaiModel.value.dataLengkap.statusPegawai ??
                    '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Prediksi Pensiun'),
                subtitle: Text(pegawaiController.full
                        .format(pegawaiController
                            .pegawaiModel.value.dataLengkap.tanggalSttb)
                        .toString() ??
                    '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Jenis Pegawai'),
                subtitle: Text(pegawaiController
                    .pegawaiModel.value.dataLengkap.jenisPegawai),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Kedudukan'),
                subtitle: Text(pegawaiController
                        .pegawaiModel.value.dataLengkap.kedudukan ??
                    '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Pangkat Terakhir'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.dataLengkap.pangkat ??
                        '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('TMT Pangkat'),
                subtitle: Text(pegawaiController.full
                    .format(pegawaiController
                        .pegawaiModel.value.dataLengkap.tmtSkPangkat)
                    .toString()),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Jabatan Terakhir'),
                subtitle: Text(pegawaiController
                        .pegawaiModel.value.dataLengkap.judulJabatan ??
                    '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('TMT Jabatan'),
                subtitle: Text(pegawaiController.full
                    .format(pegawaiController
                        .pegawaiModel.value.dataLengkap.tmtSkPangkat)
                    .toString()),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Eselon'),
                subtitle: Text(
                    pegawaiController.pegawaiModel.value.dataLengkap.eselon ??
                        '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('TMT Eselon'),
                subtitle: Text(pegawaiController.full
                    .format(pegawaiController
                        .pegawaiModel.value.dataLengkap.tmtEselon)
                    .toString()),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Pendidikan Terakhir'),
                subtitle: Text(pegawaiController
                        .pegawaiModel.value.dataLengkap.tingkatPendidikan ??
                    '-'),
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Tahun Lulus'),
                subtitle: Text(pegawaiController
                        .pegawaiModel.value.dataLengkap.tingkatPendidikan ??
                    '-'),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
