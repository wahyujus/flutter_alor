import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/AuthController.dart';
import 'package:flutter_alor/controllers/HomeController.dart';
import 'package:flutter_alor/ui/UbahPassword.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';
import 'package:get/get.dart';

import 'Pegawai/DataPegawaiLengkap.dart';

class Dashboard extends StatelessWidget {
  final homeController = Get.put(HomeController());
  final authController = Get.put(AuthController());

  @override
  Widget build(BuildContext context) {
    return Obx(
      () => SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.all(10.0),
          child: Column(
            children: [
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 10.0),
                    child: Text(
                      'Data Pegawai',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 2,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Column(
                      children: [
                        ListTile(
                          title: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              SizedBox(height: 10),
                              Text('Nama'),
                              buildText(
                                  authController.userModel.value.namaLengkap ??
                                      '-'),
                              SizedBox(height: 10),
                              Text('NIP'),
                              buildText(
                                  authController.userModel.value.nip ?? '-'),
                              SizedBox(height: 10),
                              Text('Pangkat & Golongan'),
                              buildText(
                                  '${authController.userModel.value.pangkat ?? '-'} & ${authController.userModel.value.golongan ?? '-'}'),
                              SizedBox(height: 10),
                              Text('Eselon'),
                              buildText(
                                  authController.userModel.value.eselon ?? '-'),
                              SizedBox(height: 10),
                              Text('Nama Satuan Kerja Anak'),
                              buildText(authController
                                      .userModel.value.satuanKerjaAnak ??
                                  '-'),
                              SizedBox(height: 10),
                            ],
                          ),
                          trailing: PopupMenuButton(
                            child: Icon(CupertinoIcons.ellipsis_vertical),
                            itemBuilder: (value) => <PopupMenuItem<String>>[
                              new PopupMenuItem<String>(
                                  textStyle: TextStyle(
                                      fontSize: 12, color: Colors.black),
                                  child: Text('Ubah Password'),
                                  value: 'Ubah Password'),
                              new PopupMenuItem<String>(
                                  textStyle: TextStyle(
                                      fontSize: 12, color: Colors.black),
                                  child: Text('Lihat Account'),
                                  value: 'Lihat Account'),
                            ],
                            onSelected: (value) {
                              print('object $value');
                              if (value == 'Lihat Account') {
                                Get.to(() => DataPegawaiLengkap());
                              } else {
                                Get.to(() => UbahPassword());
                              }
                            },
                          ),
                        ),
                        InkWell(
                          onTap: () => Get.to(() => DataPegawaiLengkap()),
                          child: Container(
                            padding: EdgeInsets.symmetric(
                                horizontal: 15, vertical: 8),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.only(
                                  bottomLeft: Radius.circular(10),
                                  bottomRight: Radius.circular(10)),
                              color: Colors.blue,
                            ),
                            width: double.infinity,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: [
                                Text('Data Pegawai Lengkap',
                                    style: TextStyle(color: Colors.white)),
                                Icon(
                                  Icons.chevron_right,
                                  color: Colors.white,
                                )
                              ],
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
              SizedBox(height: 10),
              StaggeredGridView.countBuilder(
                physics: NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                crossAxisCount: 4,
                itemCount: homeController.titleMenu.length,
                itemBuilder: (BuildContext context, int index) => InkWell(
                  onTap: () => Get.to(() => homeController.inkMenus(index),
                      transition: Transition.native),
                  child: Container(
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(10),
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey.withOpacity(0.5),
                          spreadRadius: 2,
                          blurRadius: 3,
                          offset: Offset(0, 1), // changes position of shadow
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Hero(
                              tag: index,
                              child: homeController.iconMenu[index]),
                          SizedBox(height: 5),
                          Text(
                            homeController.titleMenu[index],
                            style: TextStyle(fontWeight: FontWeight.w500),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                staggeredTileBuilder: (int index) =>
                    new StaggeredTile.extent(2, 80),
                mainAxisSpacing: 10.0,
                crossAxisSpacing: 10.0,
              )
            ],
          ),
        ),
      ),
    );
  }

  Text buildText(String text) {
    return Text(
      text,
      style: TextStyle(fontSize: 14, color: Colors.grey[600]),
    );
  }
}
