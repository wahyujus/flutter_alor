import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:flutter_alor/helpers/CustomLoading.dart';
import 'package:get/get.dart';

class RiwayatGajiTerakhir extends StatefulWidget {
  const RiwayatGajiTerakhir(
      {Key key,
      @required this.title,
      @required this.pageIndex,
      @required this.pageIcon})
      : super(key: key);

  final String title;
  final int pageIndex;
  final IconData pageIcon;

  @override
  _RiwayatGajiTerakhirState createState() => _RiwayatGajiTerakhirState();
}

class _RiwayatGajiTerakhirState extends State<RiwayatGajiTerakhir> {
  final pegawaiController = Get.put(PegawaiController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: Text(widget.title),
      ),
      body: Obx(
        () => pegawaiController.riwayatPegawaiModelStream.gaji == null
            ? CustomLoading()
            : RefreshIndicator(
                onRefresh: () async {
                  await pegawaiController.fetchRiwayatPegawai();
                },
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 8),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10)),
                                    color: Colors.blue,
                                  ),
                                  width: double.infinity,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('NOMOR SK :',
                                              style: TextStyle(
                                                  color: Colors.white)),
                                          Text(
                                              pegawaiController
                                                      .riwayatPegawaiModelStream
                                                      .gaji
                                                      .isEmpty
                                                  ? '-'
                                                  : pegawaiController
                                                      .riwayatPegawaiModelStream
                                                      .gaji[0]
                                                      .noSk,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontStyle: FontStyle.italic)),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('TMT SK :',
                                              style: TextStyle(
                                                  color: Colors.white)),
                                          Text(
                                              pegawaiController
                                                      .riwayatPegawaiModelStream
                                                      .gaji
                                                      .isEmpty
                                                  ? '-'
                                                  : pegawaiController.formatter
                                                      .format(pegawaiController
                                                          .riwayatPegawaiModelStream
                                                          .gaji[0]
                                                          .tanggalSk),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontStyle: FontStyle.italic)),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                ListTile(
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 10),
                                      Text('Pangkat & Golongan :'),
                                      buildText(pegawaiController
                                              .riwayatPegawaiModelStream
                                              .gaji
                                              .isEmpty
                                          ? '-'
                                          : pegawaiController
                                              .riwayatPegawaiModelStream
                                              .gaji[0]
                                              .pangkat),
                                      SizedBox(height: 10),
                                      Text('Gaji Pokok :'),
                                      Row(
                                        children: [
                                          buildText(pegawaiController
                                                  .riwayatPegawaiModelStream
                                                  .gaji
                                                  .isEmpty
                                              ? '-'
                                              : 'Rp. ${pegawaiController.currency.format(int.parse(pegawaiController.riwayatPegawaiModelStream.gaji[0].gajiPokok))}'),
                                          Icon(
                                            Icons.trending_up,
                                            color: Colors.green,
                                          )
                                        ],
                                      ),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                  trailing: Hero(
                                    tag: widget.pageIndex,
                                    child: Icon(
                                      widget.pageIcon,
                                      color: Colors.green,
                                      size: 100,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Riwayat',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              PopupMenuButton(
                                child: Icon(CupertinoIcons.sort_down),
                                itemBuilder: (value) => <PopupMenuItem<String>>[
                                  new PopupMenuItem<String>(
                                      textStyle: TextStyle(
                                          fontSize: 12, color: Colors.black),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text('Ascending'),
                                          Icon(
                                            CupertinoIcons.sort_up,
                                            color: Colors.green,
                                          )
                                        ],
                                      ),
                                      value: 'Edit Account'),
                                  new PopupMenuItem<String>(
                                      textStyle: TextStyle(
                                          fontSize: 12, color: Colors.black),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text('Descending'),
                                          Icon(
                                            CupertinoIcons.sort_down,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                      value: 'Lihat Account'),
                                ],
                                onSelected: (value) {
                                  print('object $value');
                                },
                              )
                            ],
                          ),
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height / 2,
                          child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: pegawaiController
                                .riwayatPegawaiModelStream.gaji.length,
                            itemBuilder: (BuildContext context, int index) {
                              var listItem = pegawaiController
                                  .riwayatPegawaiModelStream.gaji[index];
                              return Card(
                                child: ListTile(
                                  leading: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        pegawaiController.day
                                            .format(listItem.tmtSk),
                                        style: TextStyle(fontSize: 30),
                                      ),
                                      Text(pegawaiController.month
                                          .format(listItem.tmtSk)),
                                    ],
                                  ),
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 10),
                                      Text('Pangkat & Golongan :'),
                                      buildText(
                                          '${listItem.pangkat} & ${listItem.golongan}'),
                                      SizedBox(height: 10),
                                      Text('Gaji Pokok :'),
                                      buildText(
                                          'Rp. ${pegawaiController.currency.format(int.parse(listItem.gajiPokok))}'),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                  trailing: Icon(
                                    Icons.trending_up,
                                    color: Colors.green,
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ),
      ),
    );
  }

  Text buildText(String text) {
    return Text(
      text,
      style: TextStyle(fontSize: 14, color: Colors.grey[600]),
    );
  }
}
