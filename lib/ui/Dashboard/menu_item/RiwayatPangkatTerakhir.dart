import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:flutter_alor/helpers/CustomLoading.dart';
import 'package:get/get.dart';

class RiwayatPangkatTerakhir extends StatefulWidget {
  const RiwayatPangkatTerakhir(
      {Key key,
      @required this.title,
      @required this.pageIndex,
      @required this.pageIcon})
      : super(key: key);

  final String title;
  final int pageIndex;
  final IconData pageIcon;

  @override
  _RiwayatPangkatTerakhirState createState() => _RiwayatPangkatTerakhirState();
}

class _RiwayatPangkatTerakhirState extends State<RiwayatPangkatTerakhir> {
  final pegawaiController = Get.put(PegawaiController());

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(widget.title),
        ),
        body: Obx(() => pegawaiController.riwayatPegawaiModelStream.pangkat ==
                null
            ? CustomLoading()
            : RefreshIndicator(
                onRefresh: () async {
                  await pegawaiController.fetchRiwayatPegawai();
                },
                child: SingleChildScrollView(
                  child: Padding(
                    padding: const EdgeInsets.all(10.0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(bottom: 10.0),
                          child: Container(
                            width: double.infinity,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey.withOpacity(0.5),
                                  spreadRadius: 2,
                                  blurRadius: 2,
                                  offset: Offset(
                                      0, 1), // changes position of shadow
                                ),
                              ],
                            ),
                            child: Column(
                              children: [
                                Container(
                                  padding: EdgeInsets.symmetric(
                                      horizontal: 15, vertical: 8),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.only(
                                        topLeft: Radius.circular(10),
                                        topRight: Radius.circular(10)),
                                    color: Colors.blue,
                                  ),
                                  width: double.infinity,
                                  child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('NOMOR SK :',
                                              style: TextStyle(
                                                  color: Colors.white)),
                                          Text(
                                              pegawaiController
                                                          .riwayatPegawaiModelStream
                                                          .pangkat ==
                                                      null
                                                  ? '-'
                                                  : pegawaiController
                                                      .riwayatPegawaiModelStream
                                                      .pangkat[0]
                                                      .noSk,
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontStyle: FontStyle.italic)),
                                        ],
                                      ),
                                      Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text('TMT SK :',
                                              style: TextStyle(
                                                  color: Colors.white)),
                                          Text(
                                              pegawaiController
                                                          .riwayatPegawaiModelStream
                                                          .pangkat ==
                                                      null
                                                  ? '-'
                                                  : pegawaiController.formatter
                                                      .format(pegawaiController
                                                          .riwayatPegawaiModelStream
                                                          .pangkat[0]
                                                          .tanggalSk),
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontStyle: FontStyle.italic)),
                                        ],
                                      ),
                                    ],
                                  ),
                                ),
                                ListTile(
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 10),
                                      Text('Pangkat & Golongan :'),
                                      buildText(
                                          '${pegawaiController.riwayatPegawaiModelStream.pangkat[0].pangkat} & ${pegawaiController.riwayatPegawaiModelStream.pangkat[0].golongan}'),
                                      SizedBox(height: 10),
                                      Text('Jenis Kenaikan :'),
                                      buildText(pegawaiController
                                          .riwayatPegawaiModelStream
                                          .pangkat[0]
                                          .jenisKenaikan),
                                      SizedBox(height: 10),
                                      Text('Penjabat Penetap :'),
                                      buildText(pegawaiController
                                          .riwayatPegawaiModelStream
                                          .pangkat[0]
                                          .pejabatPenetap),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                  trailing: Hero(
                                    tag: widget.pageIndex,
                                    child: Icon(
                                      widget.pageIcon,
                                      color: Colors.purple,
                                      size: 100,
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.symmetric(vertical: 10.0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text(
                                'Riwayat',
                                style: TextStyle(fontWeight: FontWeight.bold),
                              ),
                              PopupMenuButton(
                                child: Icon(CupertinoIcons.sort_down),
                                itemBuilder: (value) => <PopupMenuItem<String>>[
                                  new PopupMenuItem<String>(
                                      textStyle: TextStyle(
                                          fontSize: 12, color: Colors.black),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text('Ascending'),
                                          Icon(
                                            CupertinoIcons.sort_up,
                                            color: Colors.green,
                                          )
                                        ],
                                      ),
                                      value: 'Ascending'),
                                  new PopupMenuItem<String>(
                                      textStyle: TextStyle(
                                          fontSize: 12, color: Colors.black),
                                      child: Row(
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: [
                                          Text('Descending'),
                                          Icon(
                                            CupertinoIcons.sort_down,
                                            color: Colors.red,
                                          )
                                        ],
                                      ),
                                      value: 'Descending'),
                                ],
                                onSelected: (value) async {
                                  print('object $value');
                                  if (value == 'Descending') {
                                    pegawaiController.filterBtn.value = true;
                                    await pegawaiController
                                        .fetchRiwayatPegawai();
                                  } else {
                                    pegawaiController.filterBtn.value = false;
                                    await pegawaiController
                                        .fetchRiwayatPegawai();
                                  }
                                },
                              )
                            ],
                          ),
                        ),
                        Container(
                          // height: MediaQuery.of(context).size.height / 2,
                          child: ListView.builder(
                            physics: NeverScrollableScrollPhysics(),
                            shrinkWrap: true,
                            itemCount: pegawaiController
                                .riwayatPegawaiModelStream.pangkat.length,
                            itemBuilder: (BuildContext context, int index) {
                              var listItem =
                                  pegawaiController.filterBtn.value == false
                                      ? pegawaiController
                                          .riwayatPegawaiModelStream
                                          .pangkat[index]
                                      : pegawaiController
                                          .riwayatPegawaiModelStream
                                          .pangkat
                                          .reversed
                                          .toList()[index];

                              return Card(
                                child: ListTile(
                                  leading: Column(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text(
                                        pegawaiController.day
                                            .format(listItem.tmtSk),
                                        style: TextStyle(fontSize: 30),
                                      ),
                                      Text(pegawaiController.month
                                          .format(listItem.tmtSk)),
                                    ],
                                  ),
                                  title: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      SizedBox(height: 10),
                                      Text('Pangkat & Golongan :'),
                                      buildText(
                                          '${listItem.pangkat} & ${listItem.golongan}'),
                                      SizedBox(height: 10),
                                      Text('Jenis Kenaikan :'),
                                      buildText(listItem.jenisKenaikan),
                                      SizedBox(height: 10),
                                    ],
                                  ),
                                  trailing: Icon(
                                    listItem.jenisKenaikan.toLowerCase() !=
                                            'reguler'
                                        ? Icons.arrow_downward
                                        : Icons.arrow_upward,
                                    color:
                                        listItem.jenisKenaikan.toLowerCase() !=
                                                'reguler'
                                            ? Colors.blue
                                            : Colors.green,
                                  ),
                                ),
                              );
                            },
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              )));
  }

  Text buildText(String text) {
    return Text(
      text,
      style: TextStyle(fontSize: 14, color: Colors.grey[600]),
    );
  }
}
