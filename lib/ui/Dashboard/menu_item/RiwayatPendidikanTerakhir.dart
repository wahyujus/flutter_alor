import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:flutter_alor/helpers/CustomLoading.dart';
import 'package:get/get.dart';

class RiwayatPendidikanTerakhir extends StatefulWidget {
  const RiwayatPendidikanTerakhir(
      {Key key,
      @required this.title,
      @required this.pageIndex,
      @required this.pageIcon})
      : super(key: key);

  final String title;
  final int pageIndex;
  final IconData pageIcon;

  @override
  _RiwayatPendidikanTerakhirState createState() =>
      _RiwayatPendidikanTerakhirState();
}

class _RiwayatPendidikanTerakhirState extends State<RiwayatPendidikanTerakhir> {
  final pegawaiController = Get.put(PegawaiController());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text(widget.title),
        ),
        body: Obx(
          () => pegawaiController.riwayatPegawaiModelStream.gaji == null
              ? CustomLoading()
              : RefreshIndicator(
                  onRefresh: () async {
                    await pegawaiController.fetchRiwayatPegawai();
                  },
                  child: SingleChildScrollView(
                    child: Padding(
                      padding: const EdgeInsets.all(10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(bottom: 10.0),
                            child: Container(
                              width: double.infinity,
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(10),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.5),
                                    spreadRadius: 2,
                                    blurRadius: 2,
                                    offset: Offset(
                                        0, 1), // changes position of shadow
                                  ),
                                ],
                              ),
                              child: Column(
                                children: [
                                  Container(
                                    padding: EdgeInsets.symmetric(
                                        horizontal: 15, vertical: 8),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(10),
                                          topRight: Radius.circular(10)),
                                      color: Colors.blue,
                                    ),
                                    width: double.infinity,
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text('NOMOR STTB :',
                                                style: TextStyle(
                                                    color: Colors.white)),
                                            Text(
                                                pegawaiController
                                                        .riwayatPegawaiModelStream
                                                        .pendidikan
                                                        .isEmpty
                                                    ? '-'
                                                    : pegawaiController
                                                        .riwayatPegawaiModelStream
                                                        .pendidikan[0]
                                                        .noSttb,
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontStyle:
                                                        FontStyle.italic)),
                                          ],
                                        ),
                                        Column(
                                          crossAxisAlignment:
                                              CrossAxisAlignment.start,
                                          children: [
                                            Text('TANGGAL STTB :',
                                                style: TextStyle(
                                                    color: Colors.white)),
                                            Text(
                                                pegawaiController
                                                        .riwayatPegawaiModelStream
                                                        .pendidikan
                                                        .isEmpty
                                                    ? '-'
                                                    : pegawaiController
                                                        .formatter
                                                        .format(pegawaiController
                                                            .riwayatPegawaiModelStream
                                                            .pendidikan[0]
                                                            .tanggalSttb),
                                                style: TextStyle(
                                                    color: Colors.white,
                                                    fontWeight: FontWeight.bold,
                                                    fontStyle:
                                                        FontStyle.italic)),
                                          ],
                                        ),
                                      ],
                                    ),
                                  ),
                                  ListTile(
                                    title: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(height: 10),
                                        Text('Tingkat Pendidikan :'),
                                        buildText(pegawaiController
                                                .riwayatPegawaiModelStream
                                                .pendidikan
                                                .isEmpty
                                            ? '-'
                                            : pegawaiController
                                                .riwayatPegawaiModelStream
                                                .pendidikan[0]
                                                .tingkatPendidikan
                                                .toString()),
                                        SizedBox(height: 10),
                                        Text('Lembaga :'),
                                        buildText(pegawaiController
                                                .riwayatPegawaiModelStream
                                                .pendidikan
                                                .isEmpty
                                            ? '-'
                                            : pegawaiController
                                                .riwayatPegawaiModelStream
                                                .pendidikan[0]
                                                .lembaga
                                                .toString()),
                                        SizedBox(height: 10),
                                        Text('Diakui :'),
                                        buildText(pegawaiController
                                                .riwayatPegawaiModelStream
                                                .pendidikan
                                                .isEmpty
                                            ? '-'
                                            : pegawaiController
                                                .riwayatPegawaiModelStream
                                                .pendidikan[0]
                                                .diakui
                                                .toString()),
                                        SizedBox(height: 10),
                                      ],
                                    ),
                                    trailing: Hero(
                                      tag: widget.pageIndex,
                                      child: Icon(
                                        widget.pageIcon,
                                        color: Colors.blue,
                                        size: 100,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
        ));
  }

  Text buildText(String text) {
    return Text(
      text,
      style: TextStyle(fontSize: 14, color: Colors.grey[600]),
    );
  }
}
