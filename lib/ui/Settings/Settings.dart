import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/AuthController.dart';
import 'package:flutter_alor/controllers/HomeController.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:flutter_alor/ui/UbahPassword.dart';
import 'package:get/get.dart';

class Settings extends StatelessWidget {
  const Settings({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final pegawaiController = Get.put(PegawaiController());
    final authController = Get.put(AuthController());
    final homeController = Get.put(HomeController());
    return Obx(
      () => Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Column(
            children: [
              Card(
                child: ListTile(
                  leading: CircleAvatar(
                    child: Icon(CupertinoIcons.person_alt),
                  ),
                  title: Text(
                      pegawaiController.pegawaiModel.value.namaLengkap ?? '-'),
                  subtitle:
                      Text(pegawaiController.pegawaiModel.value.nip ?? '-'),
                ),
              ),
              InkWell(
                onTap: () => Get.to(() => UbahPassword()),
                child: Card(
                  child: ListTile(
                    title: Text('Password'),
                    subtitle: Text('Formulir Ubah Password'),
                    trailing: Icon(Icons.chevron_right),
                  ),
                ),
              ),
            ],
          ),
          InkWell(
            onTap: () {
              showCupertinoModalPopup(
                context: context,
                builder: (BuildContext context) {
                  return CupertinoActionSheet(
                    title: const Text('Apakah anda yakin untuk keluar?'),
                    actions: [
                      CupertinoActionSheetAction(
                        child: const Text(
                          'Keluar',
                          style: TextStyle(color: Colors.red),
                        ),
                        onPressed: () {
                          homeController.currentIndex.value = 0;
                          authController.logout();
                        },
                      ),
                      CupertinoActionSheetAction(
                        child: const Text('Batal'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      )
                    ],
                  );
                },
              );
            },
            child: Card(
              color: Colors.red[600],
              child: ListTile(
                title: Text(
                  'Logout',
                  style: TextStyle(color: Colors.white),
                ),
                subtitle: Text('Keluar dari Aplikasi',
                    style: TextStyle(color: Colors.white)),
                trailing: Icon(
                  Icons.logout,
                  color: Colors.white,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
