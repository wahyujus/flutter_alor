import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/controllers/AuthController.dart';
import 'package:get/get.dart';

import 'Background.dart';

class Login extends GetWidget<AuthController> {
  final TextEditingController usernameController = TextEditingController();
  final TextEditingController passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Background(
            child: Padding(
              padding: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    // color: Color(0xFF2D665F),
                    width: 250,
                    child: Image.asset(
                      'assets/download.png',
                      fit: BoxFit.fitHeight,
                    ),
                  ),
                  Container(
                    child: Column(
                      children: [
                        TextField(
                          controller: usernameController,
                          decoration: InputDecoration(
                            labelStyle: TextStyle(color: Colors.grey),
                            suffixIcon: Icon(
                              CupertinoIcons.person_crop_circle_fill,
                            ),
                            labelText: 'Username',
                            enabledBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.grey)),
                            focusedBorder: UnderlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue)),
                          ),
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        Obx(() => TextField(
                              controller: passwordController,
                              decoration: InputDecoration(
                                labelStyle: TextStyle(color: Colors.grey),
                                suffixIcon: IconButton(
                                    icon: controller.hidePwd.value
                                        ? Icon(
                                            CupertinoIcons.eye_slash_fill,
                                            color: Colors.grey,
                                          )
                                        : Icon(
                                            CupertinoIcons.eye_fill,
                                          ),
                                    onPressed: () =>
                                        controller.hidePwd.toggle()),
                                labelText: 'Password',
                                enabledBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.grey)),
                                focusedBorder: UnderlineInputBorder(
                                    borderSide: BorderSide(color: Colors.blue)),
                              ),
                              obscureText: controller.hidePwd.value,
                            )),
                        SizedBox(height: 20),
                        MaterialButton(
                          shape: RoundedRectangleBorder(
                            borderRadius: new BorderRadius.circular(5),
                          ),
                          // color: Color(0xFF2D665F),
                          color: Colors.blue,
                          minWidth: MediaQuery.of(context).size.width,
                          height: 50.0,
                          child: Text(
                            'Masuk',
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                          onPressed: () async {
                            print(
                                '${usernameController.text}, ${passwordController.text}');
                            controller.postLogin(usernameController.text,
                                passwordController.text);

                            //Send to API
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
