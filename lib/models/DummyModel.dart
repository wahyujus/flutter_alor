// To parse this JSON data, do
//
//     final dummyModel = dummyModelFromJson(jsonString);

import 'dart:convert';

DummyModel dummyModelFromJson(String str) =>
    DummyModel.fromJson(json.decode(str));

String dummyModelToJson(DummyModel data) => json.encode(data.toJson());

class DummyModel {
  DummyModel({
    this.list,
    this.message,
    this.error,
  });

  List<ListElement> list;
  String message;
  bool error;

  factory DummyModel.fromJson(Map<String, dynamic> json) => DummyModel(
        list: List<ListElement>.from(
            json["list"].map((x) => ListElement.fromJson(x))),
        message: json["message"],
        error: json["error"],
      );

  Map<String, dynamic> toJson() => {
        "list": List<dynamic>.from(list.map((x) => x.toJson())),
        "message": message,
        "error": error,
      };
}

class ListElement {
  ListElement({
    this.satKerId,
    this.satKer,
    this.nip,
    this.nama,
    this.pangkatId,
    this.kodeWilayah,
    this.kodeSurat,
  });

  String satKerId;
  String satKer;
  String nip;
  String nama;
  String pangkatId;
  String kodeWilayah;
  String kodeSurat;

  factory ListElement.fromJson(Map<String, dynamic> json) => ListElement(
        satKerId: json["SatKerID"],
        satKer: json["SatKer"],
        nip: json["NIP"] == null ? null : json["NIP"],
        nama: json["Nama"] == null ? null : json["Nama"],
        pangkatId: json["PangkatID"] == null ? null : json["PangkatID"],
        kodeWilayah: json["KodeWilayah"] == null ? null : json["KodeWilayah"],
        kodeSurat: json["KodeSurat"] == null ? null : json["KodeSurat"],
      );

  Map<String, dynamic> toJson() => {
        "SatKerID": satKerId,
        "SatKer": satKer,
        "NIP": nip == null ? null : nip,
        "Nama": nama == null ? null : nama,
        "PangkatID": pangkatId == null ? null : pangkatId,
        "KodeWilayah": kodeWilayah == null ? null : kodeWilayah,
        "KodeSurat": kodeSurat == null ? null : kodeSurat,
      };
}
