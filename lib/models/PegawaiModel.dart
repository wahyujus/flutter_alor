// To parse this JSON data, do
//
//     final pegawaiModel = pegawaiModelFromJson(jsonString);

import 'dart:convert';

PegawaiModel pegawaiModelFromJson(String str) =>
    PegawaiModel.fromJson(json.decode(str));

String pegawaiModelToJson(PegawaiModel data) => json.encode(data.toJson());

class PegawaiModel {
  PegawaiModel({
    this.id,
    this.idLama,
    this.nip,
    this.nipLama,
    this.namaLengkap,
    this.gelarDepan,
    this.gelarBelakang,
    this.jenisKelamin,
    this.agama,
    this.tempatLahir,
    this.tanggalLahir,
    this.statusNikah,
    this.sukuBangsa,
    this.golonganDarah,
    this.alamat,
    this.provinsi,
    this.kabupaten,
    this.kecamatan,
    this.desa,
    this.kodepos,
    this.tanggalSumpah,
    this.jenisPegawaiId,
    this.statusPegawaiId,
    this.levelAksesId,
    this.password,
    this.flagPegawai,
    this.createdAt,
    this.updatedAt,
    this.kedudukanId,
    this.idGaji,
    this.idPendidikan,
    this.idPangkat,
    this.idJabatan,
    this.idCuti,
    this.idHukuman,
    this.idNikah,
    this.alamatRt,
    this.alamatRw,
    this.kodesatker,
    this.namasatker,
    this.idPensiun,
    this.idDiklat,
    this.idNonformal,
    this.penyebabMeninggal,
    this.usiaMeninggal,
    this.tanggalMeninggal,
    this.waktuMeninggal,
    this.nipAdmin,
    this.flagAdmin,
    this.fileSuratTugas,
    this.telpAdmin,
    this.modby,
    this.email,
    this.sumberData,
    this.token,
    this.tanggalLahirLengkap,
    this.dataLengkap,
  });

  int id;
  dynamic idLama;
  String nip;
  String nipLama;
  String namaLengkap;
  String gelarDepan;
  String gelarBelakang;
  String jenisKelamin;
  String agama;
  String tempatLahir;
  DateTime tanggalLahir;
  String statusNikah;
  String sukuBangsa;
  String golonganDarah;
  String alamat;
  String provinsi;
  String kabupaten;
  String kecamatan;
  String desa;
  String kodepos;
  dynamic tanggalSumpah;
  String jenisPegawaiId;
  String statusPegawaiId;
  String levelAksesId;
  String password;
  String flagPegawai;
  dynamic createdAt;
  DateTime updatedAt;
  String kedudukanId;
  String idGaji;
  String idPendidikan;
  String idPangkat;
  String idJabatan;
  dynamic idCuti;
  dynamic idHukuman;
  String idNikah;
  String alamatRt;
  String alamatRw;
  String kodesatker;
  String namasatker;
  String idPensiun;
  String idDiklat;
  dynamic idNonformal;
  dynamic penyebabMeninggal;
  dynamic usiaMeninggal;
  dynamic tanggalMeninggal;
  dynamic waktuMeninggal;
  dynamic nipAdmin;
  dynamic flagAdmin;
  dynamic fileSuratTugas;
  dynamic telpAdmin;
  String modby;
  dynamic email;
  dynamic sumberData;
  String token;
  String tanggalLahirLengkap;
  DataLengkap dataLengkap;

  factory PegawaiModel.fromJson(Map<String, dynamic> json) => PegawaiModel(
        id: json["id"],
        idLama: json["id_lama"],
        nip: json["nip"],
        nipLama: json["nip_lama"],
        namaLengkap: json["nama_lengkap"],
        gelarDepan: json["gelar_depan"],
        gelarBelakang: json["gelar_belakang"],
        jenisKelamin: json["jenis_kelamin"],
        agama: json["agama"],
        tempatLahir: json["tempat_lahir"],
        tanggalLahir: DateTime.parse(json["tanggal_lahir"]),
        statusNikah: json["status_nikah"],
        sukuBangsa: json["suku_bangsa"],
        golonganDarah: json["golongan_darah"],
        alamat: json["alamat"],
        provinsi: json["provinsi"],
        kabupaten: json["kabupaten"],
        kecamatan: json["kecamatan"],
        desa: json["desa"],
        kodepos: json["kodepos"],
        tanggalSumpah: json["tanggal_sumpah"],
        jenisPegawaiId: json["jenis_pegawai_id"],
        statusPegawaiId: json["status_pegawai_id"],
        levelAksesId: json["level_akses_id"],
        password: json["password"],
        flagPegawai: json["flag_pegawai"],
        createdAt: json["created_at"],
        updatedAt: DateTime.parse(json["updated_at"]),
        kedudukanId: json["kedudukan_id"],
        idGaji: json["id_gaji"],
        idPendidikan: json["id_pendidikan"],
        idPangkat: json["id_pangkat"],
        idJabatan: json["id_jabatan"],
        idCuti: json["id_cuti"],
        idHukuman: json["id_hukuman"],
        idNikah: json["id_nikah"],
        alamatRt: json["alamat_rt"],
        alamatRw: json["alamat_rw"],
        kodesatker: json["kodesatker"],
        namasatker: json["namasatker"],
        idPensiun: json["id_pensiun"],
        idDiklat: json["id_diklat"],
        idNonformal: json["id_nonformal"],
        penyebabMeninggal: json["penyebab_meninggal"],
        usiaMeninggal: json["usia_meninggal"],
        tanggalMeninggal: json["tanggal_meninggal"],
        waktuMeninggal: json["waktu_meninggal"],
        nipAdmin: json["nip_admin"],
        flagAdmin: json["flag_admin"],
        fileSuratTugas: json["file_surat_tugas"],
        telpAdmin: json["telp_admin"],
        modby: json["modby"],
        email: json["email"],
        sumberData: json["sumber_data"],
        token: json["token"],
        tanggalLahirLengkap: json["tanggal_lahir_lengkap"],
        dataLengkap: DataLengkap.fromJson(json["data_lengkap"]),
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_lama": idLama,
        "nip": nip,
        "nip_lama": nipLama,
        "nama_lengkap": namaLengkap,
        "gelar_depan": gelarDepan,
        "gelar_belakang": gelarBelakang,
        "jenis_kelamin": jenisKelamin,
        "agama": agama,
        "tempat_lahir": tempatLahir,
        "tanggal_lahir":
            "${tanggalLahir.year.toString().padLeft(4, '0')}-${tanggalLahir.month.toString().padLeft(2, '0')}-${tanggalLahir.day.toString().padLeft(2, '0')}",
        "status_nikah": statusNikah,
        "suku_bangsa": sukuBangsa,
        "golongan_darah": golonganDarah,
        "alamat": alamat,
        "provinsi": provinsi,
        "kabupaten": kabupaten,
        "kecamatan": kecamatan,
        "desa": desa,
        "kodepos": kodepos,
        "tanggal_sumpah": tanggalSumpah,
        "jenis_pegawai_id": jenisPegawaiId,
        "status_pegawai_id": statusPegawaiId,
        "level_akses_id": levelAksesId,
        "password": password,
        "flag_pegawai": flagPegawai,
        "created_at": createdAt,
        "updated_at": updatedAt.toIso8601String(),
        "kedudukan_id": kedudukanId,
        "id_gaji": idGaji,
        "id_pendidikan": idPendidikan,
        "id_pangkat": idPangkat,
        "id_jabatan": idJabatan,
        "id_cuti": idCuti,
        "id_hukuman": idHukuman,
        "id_nikah": idNikah,
        "alamat_rt": alamatRt,
        "alamat_rw": alamatRw,
        "kodesatker": kodesatker,
        "namasatker": namasatker,
        "id_pensiun": idPensiun,
        "id_diklat": idDiklat,
        "id_nonformal": idNonformal,
        "penyebab_meninggal": penyebabMeninggal,
        "usia_meninggal": usiaMeninggal,
        "tanggal_meninggal": tanggalMeninggal,
        "waktu_meninggal": waktuMeninggal,
        "nip_admin": nipAdmin,
        "flag_admin": flagAdmin,
        "file_surat_tugas": fileSuratTugas,
        "telp_admin": telpAdmin,
        "modby": modby,
        "email": email,
        "sumber_data": sumberData,
        "token": token,
        "tanggal_lahir_lengkap": tanggalLahirLengkap,
        "data_lengkap": dataLengkap.toJson(),
      };
}

class DataLengkap {
  DataLengkap({
    this.id,
    this.idPendidikan,
    this.nip,
    this.nipLama,
    this.namaLengkap,
    this.jenisKelamin,
    this.agama,
    this.tempatLahir,
    this.tanggalLahir,
    this.statusNikah,
    this.sukuBangsa,
    this.golonganDarah,
    this.alamat,
    this.alamatRt,
    this.alamatRw,
    this.provinsi,
    this.kabupaten,
    this.kecamatan,
    this.desa,
    this.kodepos,
    this.kodesatker,
    this.password,
    this.levelAksesId,
    this.tanggalSumpah,
    this.statusPegawaiId,
    this.updatedAt,
    this.idGaji,
    this.jenisPegawai,
    this.statusPegawai,
    this.noSkGaji,
    this.tanggalSkGaji,
    this.tmtSkGaji,
    this.masaKerjaGaji,
    this.golonganGaji,
    this.gajiPokok,
    this.pejabatPenetapGaji,
    this.jenisKenaikanGaji,
    this.jenisKenaikanPangkat,
    this.jabatanId,
    this.noSkJabatan,
    this.judulJabatan,
    this.tanggalSkJabatan,
    this.tmtSkJabatan,
    this.tmtEselon,
    this.pejabatPenetapJabatan,
    this.namaJabatan,
    this.batasUsia,
    this.namaJabatanInduk,
    this.kodeJabatanInduk,
    this.batasUsiaInduk,
    this.satuanKerja,
    this.idSatuanKerjaAnak,
    this.satuanKerjaAnak,
    this.tanggalSttb,
    this.jurusan,
    this.lembaga,
    this.gelarBelakang,
    this.gelarDepan,
    this.tingkatPendidikan,
    this.namaJurusan,
    this.tingkatPendidikanId,
    this.tingkatDiklat,
    this.namaDiklat,
    this.tanggalSttpp,
    this.kedudukan,
    this.noSkPangkat,
    this.tanggalSkPangkat,
    this.tmtSkPangkat,
    this.pegawaiId,
    this.pejabatPenetapPangkat,
    this.golonganId,
    this.golongan,
    this.pangkat,
    this.eselon,
    this.eselonId,
    this.satuanKerjaId,
    this.parentKode,
    this.tmtSkPangkatLengkap,
    this.tmtSkJabatanLengkap,
    this.tmtEselonLengkap,
  });

  String id;
  String idPendidikan;
  String nip;
  String nipLama;
  String namaLengkap;
  String jenisKelamin;
  String agama;
  String tempatLahir;
  DateTime tanggalLahir;
  String statusNikah;
  String sukuBangsa;
  String golonganDarah;
  String alamat;
  String alamatRt;
  String alamatRw;
  String provinsi;
  String kabupaten;
  String kecamatan;
  String desa;
  String kodepos;
  String kodesatker;
  String password;
  String levelAksesId;
  dynamic tanggalSumpah;
  String statusPegawaiId;
  DateTime updatedAt;
  String idGaji;
  String jenisPegawai;
  String statusPegawai;
  dynamic noSkGaji;
  dynamic tanggalSkGaji;
  dynamic tmtSkGaji;
  dynamic masaKerjaGaji;
  dynamic golonganGaji;
  dynamic gajiPokok;
  dynamic pejabatPenetapGaji;
  dynamic jenisKenaikanGaji;
  String jenisKenaikanPangkat;
  String jabatanId;
  String noSkJabatan;
  String judulJabatan;
  DateTime tanggalSkJabatan;
  DateTime tmtSkJabatan;
  DateTime tmtEselon;
  String pejabatPenetapJabatan;
  String namaJabatan;
  String batasUsia;
  dynamic namaJabatanInduk;
  dynamic kodeJabatanInduk;
  dynamic batasUsiaInduk;
  String satuanKerja;
  String idSatuanKerjaAnak;
  String satuanKerjaAnak;
  DateTime tanggalSttb;
  dynamic jurusan;
  dynamic lembaga;
  dynamic gelarBelakang;
  dynamic gelarDepan;
  String tingkatPendidikan;
  dynamic namaJurusan;
  String tingkatPendidikanId;
  dynamic tingkatDiklat;
  dynamic namaDiklat;
  dynamic tanggalSttpp;
  String kedudukan;
  String noSkPangkat;
  DateTime tanggalSkPangkat;
  DateTime tmtSkPangkat;
  String pegawaiId;
  String pejabatPenetapPangkat;
  String golonganId;
  String golongan;
  String pangkat;
  String eselon;
  String eselonId;
  String satuanKerjaId;
  String parentKode;
  String tmtSkPangkatLengkap;
  String tmtSkJabatanLengkap;
  String tmtEselonLengkap;

  factory DataLengkap.fromJson(Map<String, dynamic> json) => DataLengkap(
        id: json["id"],
        idPendidikan: json["id_pendidikan"],
        nip: json["nip"],
        nipLama: json["nip_lama"],
        namaLengkap: json["nama_lengkap"],
        jenisKelamin: json["jenis_kelamin"],
        agama: json["agama"],
        tempatLahir: json["tempat_lahir"],
        tanggalLahir: DateTime.parse(json["tanggal_lahir"]),
        statusNikah: json["status_nikah"],
        sukuBangsa: json["suku_bangsa"],
        golonganDarah: json["golongan_darah"],
        alamat: json["alamat"],
        alamatRt: json["alamat_rt"],
        alamatRw: json["alamat_rw"],
        provinsi: json["provinsi"],
        kabupaten: json["kabupaten"],
        kecamatan: json["kecamatan"],
        desa: json["desa"],
        kodepos: json["kodepos"],
        kodesatker: json["kodesatker"],
        password: json["password"],
        levelAksesId: json["level_akses_id"],
        tanggalSumpah: json["tanggal_sumpah"],
        statusPegawaiId: json["status_pegawai_id"],
        updatedAt: DateTime.parse(json["updated_at"]),
        idGaji: json["id_gaji"],
        jenisPegawai: json["jenis_pegawai"],
        statusPegawai: json["status_pegawai"],
        noSkGaji: json["no_sk_gaji"],
        tanggalSkGaji: json["tanggal_sk_gaji"],
        tmtSkGaji: json["tmt_sk_gaji"],
        masaKerjaGaji: json["masa_kerja_gaji"],
        golonganGaji: json["golongan_gaji"],
        gajiPokok: json["gaji_pokok"],
        pejabatPenetapGaji: json["pejabat_penetap_gaji"],
        jenisKenaikanGaji: json["jenis_kenaikan_gaji"],
        jenisKenaikanPangkat: json["jenis_kenaikan_pangkat"],
        jabatanId: json["jabatan_id"],
        noSkJabatan: json["no_sk_jabatan"],
        judulJabatan: json["judul_jabatan"],
        tanggalSkJabatan: DateTime.parse(json["tanggal_sk_jabatan"]),
        tmtSkJabatan: DateTime.parse(json["tmt_sk_jabatan"]),
        tmtEselon: DateTime.parse(json["tmt_eselon"]),
        pejabatPenetapJabatan: json["pejabat_penetap_jabatan"],
        namaJabatan: json["nama_jabatan"],
        batasUsia: json["batas_usia"],
        namaJabatanInduk: json["nama_jabatan_induk"],
        kodeJabatanInduk: json["kode_jabatan_induk"],
        batasUsiaInduk: json["batas_usia_induk"],
        satuanKerja: json["satuan_kerja"],
        idSatuanKerjaAnak: json["id_satuan_kerja_anak"],
        satuanKerjaAnak: json["satuan_kerja_anak"],
        tanggalSttb: DateTime.parse(json["tanggal_sttb"]),
        jurusan: json["jurusan"],
        lembaga: json["lembaga"],
        gelarBelakang: json["gelar_belakang"],
        gelarDepan: json["gelar_depan"],
        tingkatPendidikan: json["tingkat_pendidikan"],
        namaJurusan: json["nama_jurusan"],
        tingkatPendidikanId: json["tingkat_pendidikan_id"],
        tingkatDiklat: json["tingkat_diklat"],
        namaDiklat: json["nama_diklat"],
        tanggalSttpp: json["tanggal_sttpp"],
        kedudukan: json["kedudukan"],
        noSkPangkat: json["no_sk_pangkat"],
        tanggalSkPangkat: DateTime.parse(json["tanggal_sk_pangkat"]),
        tmtSkPangkat: DateTime.parse(json["tmt_sk_pangkat"]),
        pegawaiId: json["pegawai_id"],
        pejabatPenetapPangkat: json["pejabat_penetap_pangkat"],
        golonganId: json["golongan_id"],
        golongan: json["golongan"],
        pangkat: json["pangkat"],
        eselon: json["eselon"],
        eselonId: json["eselon_id"],
        satuanKerjaId: json["satuan_kerja_id"],
        parentKode: json["parent_kode"],
        tmtSkPangkatLengkap: json["tmt_sk_pangkat_lengkap"],
        tmtSkJabatanLengkap: json["tmt_sk_jabatan_lengkap"],
        tmtEselonLengkap: json["tmt_eselon_lengkap"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "id_pendidikan": idPendidikan,
        "nip": nip,
        "nip_lama": nipLama,
        "nama_lengkap": namaLengkap,
        "jenis_kelamin": jenisKelamin,
        "agama": agama,
        "tempat_lahir": tempatLahir,
        "tanggal_lahir":
            "${tanggalLahir.year.toString().padLeft(4, '0')}-${tanggalLahir.month.toString().padLeft(2, '0')}-${tanggalLahir.day.toString().padLeft(2, '0')}",
        "status_nikah": statusNikah,
        "suku_bangsa": sukuBangsa,
        "golongan_darah": golonganDarah,
        "alamat": alamat,
        "alamat_rt": alamatRt,
        "alamat_rw": alamatRw,
        "provinsi": provinsi,
        "kabupaten": kabupaten,
        "kecamatan": kecamatan,
        "desa": desa,
        "kodepos": kodepos,
        "kodesatker": kodesatker,
        "password": password,
        "level_akses_id": levelAksesId,
        "tanggal_sumpah": tanggalSumpah,
        "status_pegawai_id": statusPegawaiId,
        "updated_at": updatedAt.toIso8601String(),
        "id_gaji": idGaji,
        "jenis_pegawai": jenisPegawai,
        "status_pegawai": statusPegawai,
        "no_sk_gaji": noSkGaji,
        "tanggal_sk_gaji": tanggalSkGaji,
        "tmt_sk_gaji": tmtSkGaji,
        "masa_kerja_gaji": masaKerjaGaji,
        "golongan_gaji": golonganGaji,
        "gaji_pokok": gajiPokok,
        "pejabat_penetap_gaji": pejabatPenetapGaji,
        "jenis_kenaikan_gaji": jenisKenaikanGaji,
        "jenis_kenaikan_pangkat": jenisKenaikanPangkat,
        "jabatan_id": jabatanId,
        "no_sk_jabatan": noSkJabatan,
        "judul_jabatan": judulJabatan,
        "tanggal_sk_jabatan":
            "${tanggalSkJabatan.year.toString().padLeft(4, '0')}-${tanggalSkJabatan.month.toString().padLeft(2, '0')}-${tanggalSkJabatan.day.toString().padLeft(2, '0')}",
        "tmt_sk_jabatan":
            "${tmtSkJabatan.year.toString().padLeft(4, '0')}-${tmtSkJabatan.month.toString().padLeft(2, '0')}-${tmtSkJabatan.day.toString().padLeft(2, '0')}",
        "tmt_eselon":
            "${tmtEselon.year.toString().padLeft(4, '0')}-${tmtEselon.month.toString().padLeft(2, '0')}-${tmtEselon.day.toString().padLeft(2, '0')}",
        "pejabat_penetap_jabatan": pejabatPenetapJabatan,
        "nama_jabatan": namaJabatan,
        "batas_usia": batasUsia,
        "nama_jabatan_induk": namaJabatanInduk,
        "kode_jabatan_induk": kodeJabatanInduk,
        "batas_usia_induk": batasUsiaInduk,
        "satuan_kerja": satuanKerja,
        "id_satuan_kerja_anak": idSatuanKerjaAnak,
        "satuan_kerja_anak": satuanKerjaAnak,
        "tanggal_sttb":
            "${tanggalSttb.year.toString().padLeft(4, '0')}-${tanggalSttb.month.toString().padLeft(2, '0')}-${tanggalSttb.day.toString().padLeft(2, '0')}",
        "jurusan": jurusan,
        "lembaga": lembaga,
        "gelar_belakang": gelarBelakang,
        "gelar_depan": gelarDepan,
        "tingkat_pendidikan": tingkatPendidikan,
        "nama_jurusan": namaJurusan,
        "tingkat_pendidikan_id": tingkatPendidikanId,
        "tingkat_diklat": tingkatDiklat,
        "nama_diklat": namaDiklat,
        "tanggal_sttpp": tanggalSttpp,
        "kedudukan": kedudukan,
        "no_sk_pangkat": noSkPangkat,
        "tanggal_sk_pangkat":
            "${tanggalSkPangkat.year.toString().padLeft(4, '0')}-${tanggalSkPangkat.month.toString().padLeft(2, '0')}-${tanggalSkPangkat.day.toString().padLeft(2, '0')}",
        "tmt_sk_pangkat":
            "${tmtSkPangkat.year.toString().padLeft(4, '0')}-${tmtSkPangkat.month.toString().padLeft(2, '0')}-${tmtSkPangkat.day.toString().padLeft(2, '0')}",
        "pegawai_id": pegawaiId,
        "pejabat_penetap_pangkat": pejabatPenetapPangkat,
        "golongan_id": golonganId,
        "golongan": golongan,
        "pangkat": pangkat,
        "eselon": eselon,
        "eselon_id": eselonId,
        "satuan_kerja_id": satuanKerjaId,
        "parent_kode": parentKode,
        "tmt_sk_pangkat_lengkap": tmtSkPangkatLengkap,
        "tmt_sk_jabatan_lengkap": tmtSkJabatanLengkap,
        "tmt_eselon_lengkap": tmtEselonLengkap,
      };
}
