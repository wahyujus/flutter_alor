// To parse this JSON data, do
//
//     final loginModel = loginModelFromJson(jsonString);

import 'dart:convert';

LoginModel loginModelFromJson(String str) =>
    LoginModel.fromJson(json.decode(str));

String loginModelToJson(LoginModel data) => json.encode(data.toJson());

class LoginModel {
  LoginModel({
    this.type,
    this.token,
    this.nip,
    this.message,
  });

  String type;
  String token;
  String nip;
  String message;

  factory LoginModel.fromJson(Map<String, dynamic> json) => LoginModel(
        type: json["type"],
        token: json["token"],
        nip: json["nip"],
        message: json["message"],
      );

  Map<String, dynamic> toJson() => {
        "type": type,
        "token": token,
        "nip": nip,
        "message": message,
      };
}
