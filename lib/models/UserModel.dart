// To parse this JSON data, do
//
//     final userModel = userModelFromJson(jsonString);

import 'dart:convert';

UserModel userModelFromJson(String str) => UserModel.fromJson(json.decode(str));

String userModelToJson(UserModel data) => json.encode(data.toJson());

class UserModel {
  UserModel({
    this.nip,
    this.namaLengkap,
    this.eselon,
    this.pangkat,
    this.golongan,
    this.gelarDepan,
    this.gelarBelakang,
    this.satuanKerjaAnak,
  });

  String nip;
  String namaLengkap;
  String eselon;
  String pangkat;
  String golongan;
  dynamic gelarDepan;
  dynamic gelarBelakang;
  String satuanKerjaAnak;

  factory UserModel.fromJson(Map<String, dynamic> json) => UserModel(
        nip: json["nip"],
        namaLengkap: json["nama_lengkap"],
        eselon: json["eselon"],
        pangkat: json["pangkat"],
        golongan: json["golongan"],
        gelarDepan: json["gelar_depan"],
        gelarBelakang: json["gelar_belakang"],
        satuanKerjaAnak: json["satuan_kerja_anak"],
      );

  Map<String, dynamic> toJson() => {
        "nip": nip,
        "nama_lengkap": namaLengkap,
        "eselon": eselon,
        "pangkat": pangkat,
        "golongan": golongan,
        "gelar_depan": gelarDepan,
        "gelar_belakang": gelarBelakang,
        "satuan_kerja_anak": satuanKerjaAnak,
      };
}
