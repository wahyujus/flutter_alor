// To parse this JSON data, do
//
//     final komplainModel = komplainModelFromJson(jsonString);

import 'dart:convert';

KomplainModel komplainModelFromJson(String str) =>
    KomplainModel.fromJson(json.decode(str));

String komplainModelToJson(KomplainModel data) => json.encode(data.toJson());

class KomplainModel {
  KomplainModel({
    this.success,
    this.messages,
  });

  bool success;
  String messages;

  factory KomplainModel.fromJson(Map<String, dynamic> json) => KomplainModel(
        success: json["success"],
        messages: json["messages"],
      );

  Map<String, dynamic> toJson() => {
        "success": success,
        "messages": messages,
      };
}
