// To parse this JSON data, do
//
//     final riwayatPegawaiModel = riwayatPegawaiModelFromJson(jsonString);

import 'dart:convert';

RiwayatPegawaiModel riwayatPegawaiModelFromJson(String str) =>
    RiwayatPegawaiModel.fromJson(json.decode(str));

String riwayatPegawaiModelToJson(RiwayatPegawaiModel data) =>
    json.encode(data.toJson());

class RiwayatPegawaiModel {
  RiwayatPegawaiModel({
    this.jabatan,
    this.pangkat,
    this.gaji,
    this.pendidikan,
  });

  List<Jabatan> jabatan;
  List<Pangkat> pangkat;
  List<Gaji> gaji;
  List<Pendidikan> pendidikan;

  factory RiwayatPegawaiModel.fromJson(Map<String, dynamic> json) =>
      RiwayatPegawaiModel(
        jabatan:
            List<Jabatan>.from(json["jabatan"].map((x) => Jabatan.fromJson(x))),
        pangkat:
            List<Pangkat>.from(json["pangkat"].map((x) => Pangkat.fromJson(x))),
        gaji: List<Gaji>.from(json["gaji"].map((x) => Gaji.fromJson(x))),
        pendidikan: List<Pendidikan>.from(
            json["pendidikan"].map((x) => Pendidikan.fromJson(x))),
      );

  Map<String, dynamic> toJson() => {
        "jabatan": List<dynamic>.from(jabatan.map((x) => x.toJson())),
        "pangkat": List<dynamic>.from(pangkat.map((x) => x.toJson())),
        "gaji": List<dynamic>.from(gaji.map((x) => x.toJson())),
        "pendidikan": List<dynamic>.from(pendidikan.map((x) => x.toJson())),
      };
}

class Gaji {
  Gaji({
    this.id,
    this.noSk,
    this.tanggalSk,
    this.tmtSk,
    this.masaKerja,
    this.pegawaiId,
    this.golonganId,
    this.image,
    this.jenisKenaikanId,
    this.pejabatPenetapId,
    this.gajiPokok,
    this.golongan,
    this.pangkat,
    this.jenisKenaikan,
    this.pejabatPenetap,
  });

  String id;
  String noSk;
  DateTime tanggalSk;
  DateTime tmtSk;
  String masaKerja;
  String pegawaiId;
  String golonganId;
  String image;
  String jenisKenaikanId;
  String pejabatPenetapId;
  String gajiPokok;
  String golongan;
  String pangkat;
  String jenisKenaikan;
  String pejabatPenetap;

  factory Gaji.fromJson(Map<String, dynamic> json) => Gaji(
        id: json["id"],
        noSk: json["no_sk"],
        tanggalSk: DateTime.parse(json["tanggal_sk"]),
        tmtSk: DateTime.parse(json["tmt_sk"]),
        masaKerja: json["masa_kerja"],
        pegawaiId: json["pegawai_id"],
        golonganId: json["golongan_id"],
        image: json["image"],
        jenisKenaikanId: json["jenis_kenaikan_id"],
        pejabatPenetapId: json["pejabat_penetap_id"],
        gajiPokok: json["gaji_pokok"],
        golongan: json["golongan"],
        pangkat: json["pangkat"],
        jenisKenaikan: json["jenis_kenaikan"],
        pejabatPenetap: json["pejabat_penetap"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "no_sk": noSk,
        "tanggal_sk":
            "${tanggalSk.year.toString().padLeft(4, '0')}-${tanggalSk.month.toString().padLeft(2, '0')}-${tanggalSk.day.toString().padLeft(2, '0')}",
        "tmt_sk":
            "${tmtSk.year.toString().padLeft(4, '0')}-${tmtSk.month.toString().padLeft(2, '0')}-${tmtSk.day.toString().padLeft(2, '0')}",
        "masa_kerja": masaKerja,
        "pegawai_id": pegawaiId,
        "golongan_id": golonganId,
        "image": image,
        "jenis_kenaikan_id": jenisKenaikanId,
        "pejabat_penetap_id": pejabatPenetapId,
        "gaji_pokok": gajiPokok,
        "golongan": golongan,
        "pangkat": pangkat,
        "jenis_kenaikan": jenisKenaikan,
        "pejabat_penetap": pejabatPenetap,
      };
}

class Jabatan {
  Jabatan({
    this.id,
    this.noSk,
    this.tanggalSk,
    this.tmtSk,
    this.tmtEselon,
    this.pegawaiId,
    this.jabatanId,
    this.satuanKerjaId,
    this.eselonId,
    this.pejabatPenetapId,
    this.judulJabatan,
    this.tunjangan,
    this.kredit,
    this.noPelantikan,
    this.tanggalPelantikan,
    this.kelasJabatan,
    this.bidangStudi,
    this.image,
    this.eselon,
    this.parentKode,
    this.namaJabatan,
    this.satuanKerja,
    this.pejabatPenetap,
    this.nip,
    this.namaLengkap,
    this.gelarDepan,
    this.gelarBelakang,
    this.satuanKerjaLama,
  });

  String id;
  String noSk;
  DateTime tanggalSk;
  DateTime tmtSk;
  DateTime tmtEselon;
  String pegawaiId;
  String jabatanId;
  String satuanKerjaId;
  String eselonId;
  String pejabatPenetapId;
  String judulJabatan;
  dynamic tunjangan;
  dynamic kredit;
  dynamic noPelantikan;
  dynamic tanggalPelantikan;
  dynamic kelasJabatan;
  dynamic bidangStudi;
  String image;
  String eselon;
  String parentKode;
  String namaJabatan;
  String satuanKerja;
  String pejabatPenetap;
  String nip;
  String namaLengkap;
  dynamic gelarDepan;
  dynamic gelarBelakang;
  dynamic satuanKerjaLama;

  factory Jabatan.fromJson(Map<String, dynamic> json) => Jabatan(
        id: json["id"],
        noSk: json["no_sk"],
        tanggalSk: DateTime.parse(json["tanggal_sk"]),
        tmtSk: DateTime.parse(json["tmt_sk"]),
        tmtEselon: DateTime.parse(json["tmt_eselon"]),
        pegawaiId: json["pegawai_id"],
        jabatanId: json["jabatan_id"],
        satuanKerjaId: json["satuan_kerja_id"],
        eselonId: json["eselon_id"],
        pejabatPenetapId: json["pejabat_penetap_id"],
        judulJabatan: json["judul_jabatan"],
        tunjangan: json["tunjangan"],
        kredit: json["kredit"],
        noPelantikan: json["no_pelantikan"],
        tanggalPelantikan: json["tanggal_pelantikan"],
        kelasJabatan: json["kelas_jabatan"],
        bidangStudi: json["bidang_studi"],
        image: json["image"],
        eselon: json["eselon"],
        parentKode: json["parent_kode"],
        namaJabatan: json["nama_jabatan"],
        satuanKerja: json["satuan_kerja"],
        pejabatPenetap: json["pejabat_penetap"],
        nip: json["nip"],
        namaLengkap: json["nama_lengkap"],
        gelarDepan: json["gelar_depan"],
        gelarBelakang: json["gelar_belakang"],
        satuanKerjaLama: json["satuan_kerja_lama"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "no_sk": noSk,
        "tanggal_sk":
            "${tanggalSk.year.toString().padLeft(4, '0')}-${tanggalSk.month.toString().padLeft(2, '0')}-${tanggalSk.day.toString().padLeft(2, '0')}",
        "tmt_sk":
            "${tmtSk.year.toString().padLeft(4, '0')}-${tmtSk.month.toString().padLeft(2, '0')}-${tmtSk.day.toString().padLeft(2, '0')}",
        "tmt_eselon":
            "${tmtEselon.year.toString().padLeft(4, '0')}-${tmtEselon.month.toString().padLeft(2, '0')}-${tmtEselon.day.toString().padLeft(2, '0')}",
        "pegawai_id": pegawaiId,
        "jabatan_id": jabatanId,
        "satuan_kerja_id": satuanKerjaId,
        "eselon_id": eselonId,
        "pejabat_penetap_id": pejabatPenetapId,
        "judul_jabatan": judulJabatan,
        "tunjangan": tunjangan,
        "kredit": kredit,
        "no_pelantikan": noPelantikan,
        "tanggal_pelantikan": tanggalPelantikan,
        "kelas_jabatan": kelasJabatan,
        "bidang_studi": bidangStudi,
        "image": image,
        "eselon": eselon,
        "parent_kode": parentKode,
        "nama_jabatan": namaJabatan,
        "satuan_kerja": satuanKerja,
        "pejabat_penetap": pejabatPenetap,
        "nip": nip,
        "nama_lengkap": namaLengkap,
        "gelar_depan": gelarDepan,
        "gelar_belakang": gelarBelakang,
        "satuan_kerja_lama": satuanKerjaLama,
      };
}

class Pangkat {
  Pangkat({
    this.id,
    this.noSk,
    this.tanggalSk,
    this.tmtSk,
    this.noNota,
    this.tanggalNota,
    this.noStlud,
    this.tanggalStlud,
    this.pegawaiId,
    this.jenisKenaikanId,
    this.golonganId,
    this.pejabatPenetapId,
    this.tanggalSumpah,
    this.masaKerja,
    this.masaKerjaBl,
    this.angkaKredit,
    this.image,
    this.imageSumpah,
    this.imagePak,
    this.imageStlud,
    this.imageSpmt,
    this.imageSuket,
    this.createdAt,
    this.updatedAt,
    this.golongan,
    this.pangkat,
    this.jenisKenaikan,
    this.pejabatPenetap,
  });

  String id;
  String noSk;
  DateTime tanggalSk;
  DateTime tmtSk;
  String noNota;
  DateTime tanggalNota;
  dynamic noStlud;
  dynamic tanggalStlud;
  String pegawaiId;
  String jenisKenaikanId;
  String golonganId;
  String pejabatPenetapId;
  dynamic tanggalSumpah;
  String masaKerja;
  String masaKerjaBl;
  dynamic angkaKredit;
  String image;
  dynamic imageSumpah;
  dynamic imagePak;
  dynamic imageStlud;
  dynamic imageSpmt;
  dynamic imageSuket;
  DateTime createdAt;
  DateTime updatedAt;
  String golongan;
  String pangkat;
  String jenisKenaikan;
  String pejabatPenetap;

  factory Pangkat.fromJson(Map<String, dynamic> json) => Pangkat(
        id: json["id"],
        noSk: json["no_sk"],
        tanggalSk: DateTime.parse(json["tanggal_sk"]),
        tmtSk: DateTime.parse(json["tmt_sk"]),
        noNota: json["no_nota"],
        tanggalNota: DateTime.parse(json["tanggal_nota"]),
        noStlud: json["no_stlud"],
        tanggalStlud: json["tanggal_stlud"],
        pegawaiId: json["pegawai_id"],
        jenisKenaikanId: json["jenis_kenaikan_id"],
        golonganId: json["golongan_id"],
        pejabatPenetapId: json["pejabat_penetap_id"],
        tanggalSumpah: json["tanggal_sumpah"],
        masaKerja: json["masa_kerja"],
        masaKerjaBl: json["masa_kerja_bl"],
        angkaKredit: json["angka_kredit"],
        image: json["image"],
        imageSumpah: json["image_sumpah"],
        imagePak: json["image_pak"],
        imageStlud: json["image_stlud"],
        imageSpmt: json["image_spmt"],
        imageSuket: json["image_suket"],
        createdAt: json["created_at"] == null
            ? null
            : DateTime.parse(json["created_at"]),
        updatedAt: json["updated_at"] == null
            ? null
            : DateTime.parse(json["updated_at"]),
        golongan: json["golongan"],
        pangkat: json["pangkat"],
        jenisKenaikan: json["jenis_kenaikan"],
        pejabatPenetap: json["pejabat_penetap"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "no_sk": noSk,
        "tanggal_sk":
            "${tanggalSk.year.toString().padLeft(4, '0')}-${tanggalSk.month.toString().padLeft(2, '0')}-${tanggalSk.day.toString().padLeft(2, '0')}",
        "tmt_sk":
            "${tmtSk.year.toString().padLeft(4, '0')}-${tmtSk.month.toString().padLeft(2, '0')}-${tmtSk.day.toString().padLeft(2, '0')}",
        "no_nota": noNota,
        "tanggal_nota":
            "${tanggalNota.year.toString().padLeft(4, '0')}-${tanggalNota.month.toString().padLeft(2, '0')}-${tanggalNota.day.toString().padLeft(2, '0')}",
        "no_stlud": noStlud,
        "tanggal_stlud": tanggalStlud,
        "pegawai_id": pegawaiId,
        "jenis_kenaikan_id": jenisKenaikanId,
        "golongan_id": golonganId,
        "pejabat_penetap_id": pejabatPenetapId,
        "tanggal_sumpah": tanggalSumpah,
        "masa_kerja": masaKerja,
        "masa_kerja_bl": masaKerjaBl,
        "angka_kredit": angkaKredit,
        "image": image,
        "image_sumpah": imageSumpah,
        "image_pak": imagePak,
        "image_stlud": imageStlud,
        "image_spmt": imageSpmt,
        "image_suket": imageSuket,
        "created_at": createdAt == null ? null : createdAt.toIso8601String(),
        "updated_at": updatedAt.toIso8601String(),
        "golongan": golongan,
        "pangkat": pangkat,
        "jenis_kenaikan": jenisKenaikan,
        "pejabat_penetap": pejabatPenetap,
      };
}

class Pendidikan {
  Pendidikan({
    this.id,
    this.noSttb,
    this.tanggalSttb,
    this.lembaga,
    this.jurusan,
    this.namaKepala,
    this.diakui,
    this.saatCpns,
    this.gelarDepan,
    this.gelarBelakang,
    this.pegawaiId,
    this.image,
    this.image2,
    this.tingkatPendidikanId,
    this.tingkatPendidikan,
    this.namaJurusan,
  });

  String id;
  String noSttb;
  DateTime tanggalSttb;
  String lembaga;
  dynamic jurusan;
  String namaKepala;
  String diakui;
  dynamic saatCpns;
  dynamic gelarDepan;
  dynamic gelarBelakang;
  String pegawaiId;
  String image;
  dynamic image2;
  String tingkatPendidikanId;
  String tingkatPendidikan;
  dynamic namaJurusan;

  factory Pendidikan.fromJson(Map<String, dynamic> json) => Pendidikan(
        id: json["id"],
        noSttb: json["no_sttb"],
        tanggalSttb: DateTime.parse(json["tanggal_sttb"]),
        lembaga: json["lembaga"] == null ? null : json["lembaga"],
        jurusan: json["jurusan"],
        namaKepala: json["nama_kepala"] == null ? null : json["nama_kepala"],
        diakui: json["diakui"],
        saatCpns: json["saat_cpns"],
        gelarDepan: json["gelar_depan"],
        gelarBelakang: json["gelar_belakang"],
        pegawaiId: json["pegawai_id"],
        image: json["image"] == null ? null : json["image"],
        image2: json["image2"],
        tingkatPendidikanId: json["tingkat_pendidikan_id"],
        tingkatPendidikan: json["tingkat_pendidikan"],
        namaJurusan: json["nama_jurusan"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "no_sttb": noSttb,
        "tanggal_sttb":
            "${tanggalSttb.year.toString().padLeft(4, '0')}-${tanggalSttb.month.toString().padLeft(2, '0')}-${tanggalSttb.day.toString().padLeft(2, '0')}",
        "lembaga": lembaga == null ? null : lembaga,
        "jurusan": jurusan,
        "nama_kepala": namaKepala == null ? null : namaKepala,
        "diakui": diakui,
        "saat_cpns": saatCpns,
        "gelar_depan": gelarDepan,
        "gelar_belakang": gelarBelakang,
        "pegawai_id": pegawaiId,
        "image": image == null ? null : image,
        "image2": image2,
        "tingkat_pendidikan_id": tingkatPendidikanId,
        "tingkat_pendidikan": tingkatPendidikan,
        "nama_jurusan": namaJurusan,
      };
}
