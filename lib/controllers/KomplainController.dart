import 'dart:io';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_alor/api/RestServices.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_image_compress/flutter_image_compress.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:loading_animations/loading_animations.dart';

class KomplainController extends GetxController {
  var judulFile = ''.obs;
  var isiFile = ''.obs;
  var imageFile = ''.obs;

  void getLocalImage(ImageSource imageSource) async {
    var temp = Random();
    var dir = await Directory.systemTemp;
    var targetPath =
        dir.absolute.path + "/${temp.nextInt(9999).toString()}.jpg";
    var image = await ImagePicker.platform.pickImage(source: imageSource);
    var result = await FlutterImageCompress.compressAndGetFile(
        image.path, targetPath,
        quality: 80);

    if (image != null) {
      imageFile.value = result.path;
    }
  }

  void addKomplain(
      String nip, String judul, String isi, String attachment) async {
    if (nip.isNotEmpty &&
        judul.isNotEmpty &&
        isi.isNotEmpty &&
        attachment.isNotEmpty) {
      EasyLoading.show(
          status: 'loading...',
          indicator: LoadingBouncingGrid.square(
            borderSize: 3.0,
            size: 30.0,
            backgroundColor: Colors.white,
            duration: Duration(milliseconds: 1000),
          ));
      var response =
          await RestServices.postKomplain(nip, judul, isi, attachment);
      if (response != null) {
        EasyLoading.dismiss();
        if (response.success == true) {
          imageFile.value = '';
          judulFile.value = '';
          isiFile.value = '';
          EasyLoading.showSuccess('${response.messages}');
        } else {
          imageFile.value = '';
          judulFile.value = '';
          isiFile.value = '';
          EasyLoading.showInfo('${response.messages}');
        }
      } else {
        imageFile.value = '';
        judulFile.value = '';
        isiFile.value = '';
        EasyLoading.showError('$response');
      }
    } else {
      imageFile.value = '';
      judulFile.value = '';
      isiFile.value = '';
      EasyLoading.showError('Harap isi terlebih dahulu semua form');
    }
  }
}
