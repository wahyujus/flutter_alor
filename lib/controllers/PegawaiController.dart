import 'package:flutter_alor/api/RestServices.dart';
import 'package:flutter_alor/models/PegawaiModel.dart';
import 'package:flutter_alor/models/RiwayatPegawaiModel.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:intl/intl.dart';

class PegawaiController extends GetxController {
  Rx<PegawaiModel> pegawaiModel = PegawaiModel().obs;
  Rx<RiwayatPegawaiModel> riwayatPegawaiModel = RiwayatPegawaiModel().obs;

  //storage
  GetStorage box = GetStorage();

  // stream
  PegawaiModel get pegawaiModelStream => pegawaiModel.value;
  RiwayatPegawaiModel get riwayatPegawaiModelStream =>
      riwayatPegawaiModel.value;

  //helpers
  final DateFormat formatter = DateFormat('dd-MM-yyyy');
  final DateFormat full = DateFormat('dd MMM yyyy');
  final DateFormat day = DateFormat('DD');
  final DateFormat month = DateFormat('yMMM');

  final currency = new NumberFormat("#,##0.00", "en_US");

  var filterBtn = false.obs;

  @override
  void onInit() {
    streamBinding();
    super.onInit();
  }

  Future<void> streamBinding() async {
    await pegawaiModel.bindStream(
        Stream.fromFuture(RestServices.fetchPegawai(box.read('nip'))));
    await riwayatPegawaiModel.bindStream(
        Stream.fromFuture(RestServices.fetchRiwayatPegawai(box.read('nip'))));
  }

  void fetchPegawai() async {
    var response = await RestServices.fetchPegawai(box.read('nip'));
    if (response != null) {
      pegawaiModel.value = response;
    } else {
      print('fetchPegawai gagal : $response');
    }
  }

  void fetchRiwayatPegawai() async {
    var response = await RestServices.fetchRiwayatPegawai(box.read('nip'));
    if (response != null) {
      riwayatPegawaiModel.value = response;
    } else {
      print('fetchRiwayatPegawai gagal : $response');
    }
  }
}
