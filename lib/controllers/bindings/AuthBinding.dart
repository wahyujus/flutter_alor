import 'package:flutter_alor/controllers/AuthController.dart';
import 'package:flutter_alor/controllers/PegawaiController.dart';
import 'package:get/get.dart';

import '../HomeController.dart';

class AuthBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<HomeController>(() => HomeController());
    Get.lazyPut<AuthController>(() => AuthController());
    Get.lazyPut<PegawaiController>(() => PegawaiController());
  }
}
