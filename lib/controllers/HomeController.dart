import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_alor/models/DummyModel.dart';
import 'package:flutter_alor/ui/Dashboard/menu_item/RiwayatGajiTerakhir.dart';
import 'package:flutter_alor/ui/Dashboard/menu_item/RiwayatJabatanTerakhir.dart';
import 'package:flutter_alor/ui/Dashboard/menu_item/RiwayatPangkatTerakhir.dart';
import 'package:flutter_alor/ui/Dashboard/menu_item/RiwayatPendidikanTerakhir.dart';
import 'package:get/get.dart';
import 'package:line_icons/line_icons.dart';

class HomeController extends GetxController {
  // ***
  // local
  Rx<DummyModel> listDummy = DummyModel().obs;
  var currentIndex = 0.obs;

  // stream
  DummyModel get listDummyStream => listDummy.value;

  //menus
  var titleMenu = [
    'Riwayat Pangkat Terakhir',
    'Riwayat Jabatan Terakhir',
    'Riwayat Gaji Terakhir',
    'Riwayat Pendidikan Terakhir'
  ];

  Widget inkMenus(int index) {
    var inkMenu = [
      RiwayatPangkatTerakhir(
        title: titleMenu[index],
        pageIndex: index,
        pageIcon: iconMenu[index].icon,
      ),
      RiwayatJabatanTerakhir(
        title: titleMenu[index],
        pageIndex: index,
        pageIcon: iconMenu[index].icon,
      ),
      RiwayatGajiTerakhir(
        title: titleMenu[index],
        pageIndex: index,
        pageIcon: iconMenu[index].icon,
      ),
      RiwayatPendidikanTerakhir(
        title: titleMenu[index],
        pageIndex: index,
        pageIcon: iconMenu[index].icon,
      )
    ];
    return inkMenu[index];
  }

  List<Icon> iconMenu = [
    Icon(
      LineIcons.certificate,
      color: Colors.purple,
    ),
    Icon(
      LineIcons.identificationBadge,
      color: Colors.deepOrange,
    ),
    Icon(
      LineIcons.donate,
      color: Colors.green,
    ),
    Icon(
      LineIcons.graduationCap,
      color: Colors.blue,
    )
  ];
  //menus-->

  @override
  void onInit() {
    // streamBinding();
    super.onInit();
  }
}
