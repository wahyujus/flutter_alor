import 'package:flutter/material.dart';
import 'package:flutter_alor/api/RestServices.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:loading_animations/loading_animations.dart';

class PasswordController extends GetxController {
  var passwordLama = ''.obs;
  var passwordBaru = ''.obs;

  GetStorage box = GetStorage();

  Future<void> changePassword(String nip, String newPassword) async {
    if (nip.isNotEmpty && newPassword.isNotEmpty) {
      EasyLoading.show(
          status: 'loading...',
          indicator: LoadingBouncingGrid.square(
            borderSize: 3.0,
            size: 30.0,
            backgroundColor: Colors.white,
            duration: Duration(milliseconds: 1000),
          ));
      var response = await RestServices.postChangePassword(nip, newPassword);
      if (response != null) {
        EasyLoading.dismiss();
        if (response.error == false) {
          box.write('password', newPassword);
          EasyLoading.showSuccess('${response.message}');
        } else {
          EasyLoading.showInfo('${response.message}');
        }
      } else {
        EasyLoading.showError('$response');
      }
    } else {
      EasyLoading.showError('password kosong');
    }
  }
}
