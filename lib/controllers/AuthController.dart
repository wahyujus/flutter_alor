import 'package:flutter/material.dart';
import 'package:flutter_alor/api/RestServices.dart';
import 'package:flutter_alor/models/LoginModel.dart';
import 'package:flutter_alor/models/UserModel.dart';
import 'package:flutter_alor/ui/Home.dart';
import 'package:flutter_alor/ui/Auth/Login.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';
import 'package:loading_animations/loading_animations.dart';

class AuthController extends GetxController {
  //auth login
  Rx<LoginModel> loginModel = LoginModel().obs;
  Rx<String> userToken = ''.obs;

  //data user
  Rx<UserModel> userModel = UserModel().obs;

  //stream
  String get token => loginModel.value.token;

  //shared preference
  GetStorage box = GetStorage();

  //helpers
  var hidePwd = true.obs;

  @override
  void onInit() {
    streamBinding();
    super.onInit();
  }

  void streamBinding() async {
    if (box.read('token') == null) {
      userToken.value = box.read('token');
      print('session 1 : ${box.read('token')} = ${userToken}');
    } else {
      await reLogin(box.read('username'), box.read('password'));
      await fetchingUser(box.read('nip'));
      print(
          'session saved : ${box.read('token')} => ${loginModel.value.token} => ${box.read('nip')}');
    }
  }

  Future<void> fetchingUser(String nip) async {
    var responseUser = await RestServices.fetchUser(nip);
    userModel.value = responseUser;
  }

  Future<void> reLogin(String username, String password) async {
    var response = await RestServices.postLogin(username, password);
    if (response != null) {
      EasyLoading.dismiss();
      if (response.type != "error") {
        box.write('token', response.token);
        box.write('nip', response.nip);
        loginModel.value = response;
        await fetchingUser(box.read('nip'));
        Get.offAll(() => Home());
        EasyLoading.showSuccess(response.message);
        // EasyLoading.dismiss();
      } else {
        EasyLoading.showInfo(response.message);
      }
    } else {
      EasyLoading.showError('$response');
    }
  }

  void postLogin(String username, String password) async {
    if (username.isNotEmpty && password.isNotEmpty) {
      EasyLoading.show(
          status: 'loading...',
          indicator: LoadingBouncingGrid.square(
            borderSize: 3.0,
            size: 30.0,
            backgroundColor: Colors.white,
            duration: Duration(milliseconds: 1000),
          ));

      box.write('username', username);
      box.write('password', password);
      reLogin(username, password);
    } else {
      Get.snackbar(
        'form tidak lengkap',
        'harap isi semua form',
        colorText: Colors.white,
        snackPosition: SnackPosition.BOTTOM,
        backgroundColor: Colors.black45,
        shouldIconPulse: true,
      );
    }
  }

  void logout() {
    userToken.value = '';
    box.remove('token');
    Get.offAll(() => Login());
  }
}
