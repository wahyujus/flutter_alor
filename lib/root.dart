import 'package:flutter/material.dart';
import 'package:flutter_alor/ui/Home.dart';
import 'package:flutter_alor/ui/Auth/Login.dart';
import 'package:get/get.dart';
import 'package:get_storage/get_storage.dart';

import 'controllers/AuthController.dart';

class Root extends GetWidget<AuthController> {
  GetStorage box = GetStorage();
  @override
  Widget build(BuildContext context) {
    return controller.userToken.value == null ? Login() : Home();
  }
}
